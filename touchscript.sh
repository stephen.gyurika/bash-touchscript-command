#!/bin/bash

# Check whether the user provide exactly one filename

if [ $# -ne 1 ]; then
	echo "Please give me only one filename."
	exit 1
fi

# Assign first argument as script name

filename="$1"

# Ask for file permissions from the user

echo -n "Please give me the script permissions in octal format -> "
read file_permission_octal

if [[ "$file_permission_octal" =~ ^[0-7]{3,4}$ ]]; then
	touch $filename
	chmod $file_permission_octal $filename	
else
	echo "Invalid permission given."
	exit 1
fi
